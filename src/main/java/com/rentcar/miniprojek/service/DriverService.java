package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.Exception.ResourceNotFoundException;
import com.rentcar.miniprojek.dto.driver.DriverRequest;
import com.rentcar.miniprojek.dto.driver.DriverResponse;
import com.rentcar.miniprojek.entity.Driver;
import com.rentcar.miniprojek.repository.DriverRepository;
import com.rentcar.miniprojek.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DriverService {

    private static final String DRIVER_DOES_NOT_EXIST = "driver_does_not_exist";
    private static final String STATUS_AVAILABLE = "available";
    private static final String STATUS_INORDER = "inorder";
    private final DriverRepository driverRepository;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public Driver driverExistValidation(Integer driverId) {
        Driver driverExist = driverRepository.findById(driverId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, DRIVER_DOES_NOT_EXIST, driverId)));
        return driverExist;
    }


    public DriverResponse saveDriver(DriverRequest driverRequest){
        Driver driver = new Driver();
        driver.setDriverName(driverRequest.getDriverName());
        driver.setDrivinglicence(driverRequest.getDrivinglicence());
        driver.setDriverPrice(driverRequest.getDriverPrice());
        driver.setStatus(STATUS_AVAILABLE);
        driverRepository.save(driver);

        return DriverResponse.builder()
                .id(driver.getId())
                .driverName(driver.getDriverName())
                .drivinglicence(driver.getDrivinglicence())
                .driverPrice(driver.getDriverPrice())
                .status(driver.getStatus())
                .build();
    }

    public List<DriverResponse> getAllDriver () {
        List<DriverResponse> driverList = new ArrayList<>();
        for (Driver driver : driverRepository.findAll()) {
            driverList.add(DriverResponse.builder()
                    .id(driver.getId())
                    .driverName(driver.getDriverName())
                    .drivinglicence(driver.getDrivinglicence())
                    .driverPrice(driver.getDriverPrice())
                    .status(driver.getStatus())
                    .build()
                );
            }
            return driverList;
        }
}

