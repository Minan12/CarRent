package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.Exception.ResourceNotFoundException;
import com.rentcar.miniprojek.dto.vehicle.VehicleRequest;
import com.rentcar.miniprojek.dto.vehicle.VehicleResponse;
import com.rentcar.miniprojek.entity.Driver;
import com.rentcar.miniprojek.entity.Vehicle;
import com.rentcar.miniprojek.repository.VehicleRepository;
import com.rentcar.miniprojek.util.ExceptionMessageAccessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class VehicleService {

    private static final String VEHICLE_DOES_NOT_EXIST = "vehicle_does_not_exist";
    private static final String STATUS_AVAILABLE = "available";
    private static final String STATUS_INORDER = "inorder";
    private final VehicleRepository vehicleRepository;
    private final ExceptionMessageAccessor exceptionMessageAccessor;

    public Vehicle vehicleExistValidation(Integer vehicleId) {
        Vehicle vehicleExist = vehicleRepository.findById(vehicleId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        exceptionMessageAccessor.getMessage(null, VEHICLE_DOES_NOT_EXIST, vehicleId)));
        return vehicleExist;
    }

    public VehicleResponse saveVehicle(VehicleRequest request){
        Vehicle vehicle = new Vehicle();
        vehicle.setCarName(request.getCarName());
        vehicle.setPlatNo(request.getPlatNo());
        vehicle.setPriceCar(request.getPriceCar());
        vehicle.setImgUrl(request.getImgUrl());
        vehicle.setStatus(STATUS_AVAILABLE);
        vehicleRepository.save(vehicle);

        return VehicleResponse.builder()
                .id(vehicle.getId())
                .carName(vehicle.getCarName())
                .platNo(vehicle.getPlatNo())
                .priceCar(vehicle.getPriceCar())
                .imgUrl(vehicle.getImgUrl())
                .status(vehicle.getStatus())
                .build();
    }

    public VehicleResponse updateVehicle(Integer id, VehicleRequest vehicleRequest){
        Vehicle vehicle = vehicleRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Vehicle with id " + id + " not found"));

        vehicle.setCarName(vehicleRequest.getCarName());
        vehicle.setImgUrl(vehicleRequest.getImgUrl());
        vehicle.setPlatNo(vehicleRequest.getPlatNo());
        vehicle.setPriceCar(vehicleRequest.getPriceCar());
        vehicleRepository.save(vehicle);

        return VehicleResponse.builder()
                .id(vehicle.getId())
                .carName(vehicle.getCarName())
                .imgUrl(vehicle.getImgUrl())
                .platNo(vehicle.getPlatNo())
                .priceCar(vehicle.getPriceCar())
                .status(vehicle.getStatus())
                .build();
    }

    public List<VehicleResponse> getAllVehicle() {
        List<VehicleResponse> vehicleList = new ArrayList<>();
        for (Vehicle vehicle : vehicleRepository.findAll()) {
            vehicleList.add(VehicleResponse.builder()
                    .id(vehicle.getId())
                    .carName(vehicle.getCarName())
                    .imgUrl(vehicle.getImgUrl())
                    .platNo(vehicle.getPlatNo())
                    .priceCar(vehicle.getPriceCar())
                    .status(vehicle.getStatus())
                    .build()
            );
        }
        return vehicleList;
    }
}
