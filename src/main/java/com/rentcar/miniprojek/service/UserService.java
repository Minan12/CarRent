package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.Exception.AuthenticationException;
import com.rentcar.miniprojek.dto.login.LoginRequest;
import com.rentcar.miniprojek.dto.login.LoginResponse;
import com.rentcar.miniprojek.entity.Users;
import com.rentcar.miniprojek.repository.UserRepository;
import com.rentcar.miniprojek.util.JwToken;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    public LoginResponse login(LoginRequest request) {
        Optional<Users> optionalUser = userRepository.findByUsernameAndPassword(request.getUsername(),
                request.getPassword());

        Users u = optionalUser.orElseThrow(() -> new AuthenticationException("Incorrect username or password"));

        LoginResponse response = new LoginResponse();
        u.setToken(JwToken.generateToken(request));
        u.setTanggalLogin(new Date());
        u.setCustomer(u.getCustomer());
        response.setRole("customer");

        userRepository.save(u);

        response.setToken(u.getToken());
        response.setTanggalLogin(u.getTanggalLogin());
        return response;
    }

    public void deleteCustomer(Integer userId) {
        Optional<Users> optionalUser = userRepository.findById(userId);
        Users user = optionalUser.orElseThrow(() -> new EntityNotFoundException("User not found"));
        userRepository.delete(user);
    }



}
