package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.dto.token.TokenResponse;
import com.rentcar.miniprojek.entity.Users;
import com.rentcar.miniprojek.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TokenService {

    private final UserRepository userRepository;

    public boolean getToken(String token) {
        TokenResponse response = new TokenResponse();

        Optional<Users> getToken = userRepository.findByToken(token);
        boolean validate = getToken.isPresent() ? true : false;

        response.setValid(validate);

        if (getToken.get().getCustomer() != null){
            response.setRole("Customer");
        }

        return response.isValid();
    }
}

