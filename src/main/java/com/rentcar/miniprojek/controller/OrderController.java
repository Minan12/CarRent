package com.rentcar.miniprojek.controller;

import com.rentcar.miniprojek.dto.BaseResponse;
import com.rentcar.miniprojek.dto.order.OrderDetail;
import com.rentcar.miniprojek.dto.order.OrderRequest;
import com.rentcar.miniprojek.dto.order.OrderResponse;
import com.rentcar.miniprojek.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @PostMapping("/add")
    public ResponseEntity<OrderResponse> saveOrder(String token,@Valid @RequestBody OrderRequest request){
        OrderResponse response = orderService.addOrder(token, request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<BaseResponse<OrderResponse>> returnCar(@RequestHeader String token, @PathVariable("id") Integer id, @Valid @RequestBody OrderRequest request){
        OrderResponse response = orderService.updateOrder(token, id, request);
        return ResponseEntity.status(HttpStatus.OK).body(new BaseResponse<>(response));
    }

    @GetMapping("/{id}/detail")
    public ResponseEntity<OrderDetail> getDetail(@PathVariable("id") Integer orderId){
        OrderDetail response = orderService.getDetail(orderId);
        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<OrderResponse>> getAll(){
        List<OrderResponse> response = orderService.getAllOrder();
        return ResponseEntity.ok(response);
    }
}
