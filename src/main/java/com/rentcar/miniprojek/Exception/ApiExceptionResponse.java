package com.rentcar.miniprojek.Exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ApiExceptionResponse {
    private Boolean success;
    private List<String> messages;
    private HttpStatus status;
    private LocalDateTime time;

}