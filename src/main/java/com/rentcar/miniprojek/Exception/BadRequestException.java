package com.rentcar.miniprojek.Exception;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class BadRequestException extends RuntimeException{
    private final String errorMessage;
}
