package com.rentcar.miniprojek.dto.order;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
@Data
@AllArgsConstructor
public class OrderList {
    private String custName;
    private String driverName;
    private String carName;
    private BigDecimal priceCar;

    private Integer hours;
    private BigDecimal totalPrice;
}
