package com.rentcar.miniprojek.dto.driver;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public class DriverResponse {
    private Integer id;
    private String driverName;
    private String drivinglicence;
    private BigDecimal driverPrice;
    private String status;
}
