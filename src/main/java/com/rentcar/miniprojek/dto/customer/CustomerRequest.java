package com.rentcar.miniprojek.dto.customer;

import lombok.Builder;
import lombok.Data;
import org.springframework.validation.annotation.Validated;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@Validated
public class CustomerRequest {
    private String custName;
    private String address;
    private String contactNo;
    private String drivingLicence;
    private String username;
    private String password;
}
