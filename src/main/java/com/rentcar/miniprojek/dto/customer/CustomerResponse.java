package com.rentcar.miniprojek.dto.customer;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerResponse {
    private Integer id;
    private String custName;
    private String address;
    private String contactNo;
    private String drivingLicence;
    private String username;
    private String password;
    private String message;


}
