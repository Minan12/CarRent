package com.rentcar.miniprojek.dto.login;

import lombok.Data;

import java.util.Date;

@Data
public class LoginResponse {
    private String token;
    private Date tanggalLogin;
    private String role;
}
