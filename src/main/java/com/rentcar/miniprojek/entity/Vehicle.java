package com.rentcar.miniprojek.entity;

import jakarta.persistence.*;
import lombok.Data;
import java.math.BigDecimal;

@Data
@Entity
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String carName;
    private String platNo;
    private BigDecimal priceCar;
    private String imgUrl;
    private String status;
}
