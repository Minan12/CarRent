package com.rentcar.miniprojek.repository;

import com.rentcar.miniprojek.entity.Customer;
import com.rentcar.miniprojek.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository <Customer, Integer>{

    boolean existsByUsername(String username);

    boolean existsByContactNo(String contactNo);
}
