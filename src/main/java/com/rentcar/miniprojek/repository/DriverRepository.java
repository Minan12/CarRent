package com.rentcar.miniprojek.repository;

import com.rentcar.miniprojek.entity.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DriverRepository extends JpaRepository <Driver, Integer>{
    Driver findByDriverName(String driverName);
}
