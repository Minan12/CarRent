package com.rentcar.miniprojek.controller;

import com.rentcar.miniprojek.dto.order.OrderRequest;
import com.rentcar.miniprojek.dto.order.OrderResponse;
import com.rentcar.miniprojek.service.OrderService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class OrderControllerTest {
    @InjectMocks
    OrderController orderController;
    @Mock
    OrderService orderService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testsaveOrder() {
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VycyIsImlhdCI6MTcwMjI4NzI5MH0.iUIWcbgqN1LGWmZZWOuPgPl-8TBSaVSnE42QukMi_is750wcmdIorvV9TgBZhj5nKfuXgYTof78_aYiuwylfXQ";

        OrderRequest request =  new OrderRequest(1, 1, 1, 6);

        OrderResponse response = OrderResponse.builder()
                .id(1)
                .custName("test")
                .driverName("yudi")
                .carName("minicooper")
                .tanggalOrder(new Date())
                .priceCar(BigDecimal.valueOf(10000))
                .hours(6)
                .totalPrice(BigDecimal.valueOf(60000))
                .status("Inorder")
                .build();

        when(orderService.addOrder(anyString(),any())).thenReturn(response);

        ResponseEntity<OrderResponse> actualResponse = orderController.saveOrder(token,request);

        Assertions.assertEquals(HttpStatus.CREATED, actualResponse.getStatusCode());
        Assertions.assertEquals(response, actualResponse.getBody());
    }

    @Test
    void testGetAll(){
        List<OrderResponse> response = Collections.singletonList(new OrderResponse(1, "minan", "pito" , "minicooper",BigDecimal.valueOf(15000),2,BigDecimal.valueOf(30000),"INORDER", new Date()));

        when(orderService.getAllOrder()).thenReturn(response);
        ResponseEntity<List<OrderResponse>> responseEntity = orderController.getAll();
        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
    }
}
