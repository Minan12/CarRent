package com.rentcar.miniprojek.controller;

import com.rentcar.miniprojek.controller.LoginController;
import com.rentcar.miniprojek.dto.login.LoginRequest;
import com.rentcar.miniprojek.dto.login.LoginResponse;
import com.rentcar.miniprojek.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class LoginControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private LoginController loginController;

    @BeforeEach
    void setUp() {

        MockitoAnnotations.openMocks(this);
    }

    @Test
    void doValidLogin() throws Exception {
        LoginRequest validRequest = new LoginRequest();
        validRequest.setUsername("Username");
        validRequest.setPassword("Password");
        LoginResponse mockResponse = new LoginResponse();
        mockResponse.setToken("Token");

        when(userService.login(validRequest)).thenReturn(mockResponse);

        ResponseEntity<LoginResponse> responseEntity = loginController.login(validRequest);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(mockResponse, responseEntity.getBody());
    }
}