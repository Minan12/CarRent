package com.rentcar.miniprojek.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentcar.miniprojek.dto.vehicle.VehicleRequest;
import com.rentcar.miniprojek.dto.vehicle.VehicleResponse;
import com.rentcar.miniprojek.service.VehicleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import java.math.BigDecimal;
import java.util.Arrays;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class VehicleControllerTest {

    private MockMvc mockMvc;

    @Mock
    private VehicleService vehicleService;

    @InjectMocks
    private VehicleController vehicleController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(vehicleController).build();
    }

    @Test
    void testAddVehicle() throws Exception {
        VehicleRequest request = new VehicleRequest("UpdatedCar", "XYZ789", BigDecimal.valueOf(150), "updated-image");
        request.setCarName("Test Car");
        request.setPlatNo("ABC123");
        request.setPriceCar(BigDecimal.valueOf(10000));
        request.setImgUrl("test-url");

        VehicleResponse response = new VehicleResponse(1, "Car1", "ABC123", BigDecimal.valueOf(100), "image1" , "INORDER");
        response.setId(1);
        response.setCarName("Test Car");
        response.setPlatNo("ABC123");
        response.setPriceCar(BigDecimal.valueOf(10000));
        response.setImgUrl("test-url");
        response.setStatus("INORDER");

        when(vehicleService.saveVehicle(any(VehicleRequest.class))).thenReturn(response);

        mockMvc.perform(post("/vehicle/add")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.carName").value("Test Car"))
                .andExpect(jsonPath("$.platNo").value("ABC123"))
                .andExpect(jsonPath("$.priceCar").value(10000))
                .andExpect(jsonPath("$.imgUrl").value("test-url"));
    }

    @Test
    void testGetAllVehicles() throws Exception {
        VehicleResponse response = new VehicleResponse(1, "Car1", "ABC123", BigDecimal.valueOf(100), "image1" ,"INORDER");
        response.setId(1);
        response.setCarName("Test Car");
        response.setPlatNo("ABC123");
        response.setPriceCar(BigDecimal.valueOf(10000));
        response.setImgUrl("test-url");

        when(vehicleService.getAllVehicle()).thenReturn(Arrays.asList(response)); // Ganti List.of dengan Arrays.asList

        mockMvc.perform(get("/vehicle/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].carName").value("Test Car"))
                .andExpect(jsonPath("$[0].platNo").value("ABC123"))
                .andExpect(jsonPath("$[0].priceCar").value(10000))
                .andExpect(jsonPath("$[0].imgUrl").value("test-url"));
    }

    @Test
    void testPutVehicle() throws Exception {
        VehicleRequest request = new VehicleRequest("Car1", "ABC123", BigDecimal.valueOf(100), "image1");
        request.setCarName("Updated Car");
        request.setPlatNo("XYZ789");
        request.setPriceCar(BigDecimal.valueOf(15000));
        request.setImgUrl("updated-url");

        VehicleResponse response = new VehicleResponse(1, "Car1", "ABC123", BigDecimal.valueOf(100), "image1", "INORDER");
        response.setId(1);
        response.setCarName("Updated Car");
        response.setPlatNo("XYZ789");
        response.setPriceCar(BigDecimal.valueOf(15000));
        response.setImgUrl("updated-url");
        response.setStatus("AVAILABLE");

        when(vehicleService.updateVehicle(any(Integer.class), any(VehicleRequest.class))).thenReturn(response);

        mockMvc.perform(put("/vehicle/1")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.carName").value("Updated Car"))
                .andExpect(jsonPath("$.platNo").value("XYZ789"))
                .andExpect(jsonPath("$.priceCar").value(15000))
                .andExpect(jsonPath("$.imgUrl").value("updated-url"));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}