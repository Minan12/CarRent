package com.rentcar.miniprojek.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentcar.miniprojek.dto.driver.DriverRequest;
import com.rentcar.miniprojek.dto.driver.DriverResponse;
import com.rentcar.miniprojek.service.DriverService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class DriverControllerTest {

    private MockMvc mockMvc;

    @Mock
    private DriverService driverService;

    @InjectMocks
    private DriverController driverController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(driverController).build();
    }

    @Test
    void testAddDriver() throws Exception {
        DriverRequest request = new DriverRequest("dasdq", "ABCD123", new BigDecimal(20000));
        request.setDriverName("dsadsa");
        request.setDrivinglicence("ABC123");

        DriverResponse response = new DriverResponse(1, "dsadsa", "ABC123", new BigDecimal(20000), "INORDER");
        response.setId(1);
        response.setDriverName("dsadsa");
        response.setDrivinglicence("ABC123");

        when(driverService.saveDriver(any(DriverRequest.class))).thenReturn(response);

        mockMvc.perform(post("/driver/add")
                        .content(asJsonString(request))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.driverName").value("dsadsa"))
                .andExpect(jsonPath("$.drivinglicence").value("ABC123"));
    }

    @Test
    void testGetAllDrivers() throws Exception {
        DriverResponse response = new DriverResponse(1, "sadsa", "ABC123", new BigDecimal(20000), "INORDER");
        response.setId(1);
        response.setDriverName("dsadsa");
        response.setDrivinglicence("ABC123");

        when(driverService.getAllDriver()).thenReturn(Arrays.asList(response));

        mockMvc.perform(get("/driver/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].driverName").value("dsadsa"))
                .andExpect(jsonPath("$[0].drivinglicence").value("ABC123"));
    }

    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}