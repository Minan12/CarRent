//package com.rentcar.miniprojek.service;
//
//import com.rentcar.miniprojek.dto.login.LoginRequest;
//import com.rentcar.miniprojek.repository.CustomerRepository;
//import com.rentcar.miniprojek.repository.UserRepository;
//import com.rentcar.miniprojek.service.UserService;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.boot.test.context.SpringBootTest;
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertThrows;
//
//@ExtendWith(MockitoExtension.class)
//@SpringBootTest
//class UserServiceTest {
//
//    @InjectMocks
//    private UserService userService;
//
//    @Mock
//    private UserRepository userRepository;
//
//    @Test
//    void doInvalidLogin() {
//        // Arrange
//        LoginRequest request = new LoginRequest();
//        request.setUsername("nonexistentUser");
//        request.setPassword("nonexistentPassword");
//
//        // Act and Assert
//        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> {
//            userService.login(request);
//        });
//
//        assertEquals("User Tidak Ditemukan", exception.getMessage());
//        Mockito.verify(userRepository, Mockito.never()).save(Mockito.any());
//    }
//}
