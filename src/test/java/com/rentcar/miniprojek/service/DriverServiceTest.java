
package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.dto.driver.DriverRequest;
import com.rentcar.miniprojek.dto.driver.DriverResponse;
import com.rentcar.miniprojek.dto.vehicle.VehicleResponse;
import com.rentcar.miniprojek.entity.Driver;
import com.rentcar.miniprojek.entity.Vehicle;
import com.rentcar.miniprojek.repository.DriverRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.*;


@SpringBootTest
class DriverServiceTest {

    @Mock
    private DriverRepository driverRepository;
    @InjectMocks
    private DriverService driverService;

    @Test
    void doPositiveSaveDriver() {
        DriverRequest request = DriverRequest.builder()
                .driverName("fadil")
                .drivinglicence("A123")
                .build();

        Driver driver = new Driver();
        driver.setId(1);
        driver.setDriverName(request.getDriverName());
        driver.setDrivinglicence(request.getDrivinglicence());
        Mockito.lenient().when(driverRepository.save(driver)).thenReturn(driver);

        DriverResponse response = driverService.saveDriver(request);

        Assertions.assertEquals(response.getDriverName(), request.getDriverName());
        Assertions.assertEquals(response.getDrivinglicence(), request.getDrivinglicence());
    }


    @Test
    void testSaveDriverNegative() {
        // Arrange
        DriverRequest request = DriverRequest.builder()
                .driverName("fadil")
                .drivinglicence("A123")
                .build();

        Mockito.when(driverRepository.save(any(Driver.class))).thenReturn(null);

        // Act
        DriverResponse response = driverService.saveDriver(request);

        // Assert
        assertNotNull(response);  // Memastikan response tidak null

        // Memeriksa setiap properti response
        assertNull(response.getId());
        assertEquals("fadil", response.getDriverName());
        assertEquals("A123", response.getDrivinglicence());

        // Verify that necessary methods were called
        verify(driverRepository, times(1)).save(any(Driver.class));
    }

    @Test
    void testGetAllDriver() {
        // Arrange
        List<Driver> mockDriverList = new ArrayList<>();

        Driver driver1 = new Driver();
        driver1.setId(1);
        driver1.setDriverName("fadil");
        driver1.setDrivinglicence("ABC123");

        Driver driver2 = new Driver();
        driver2.setId(1);
        driver2.setDriverName("habib");
        driver2.setDrivinglicence("ABC456");

        mockDriverList.add(driver1);
        mockDriverList.add(driver2);

        when(driverRepository.findAll()).thenReturn(mockDriverList);

        // Act
        List<DriverResponse> result = driverService.getAllDriver();

        // Assert
        assertEquals(2, result.size());

        // Verify that necessary methods were called
        verify(driverRepository, times(1)).findAll();
    }

}
