package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.Exception.RegistrationException;
import com.rentcar.miniprojek.dto.customer.CustomerRequest;
import com.rentcar.miniprojek.dto.customer.CustomerResponse;
import com.rentcar.miniprojek.entity.Customer;
import com.rentcar.miniprojek.repository.CustomerRepository;
import com.rentcar.miniprojek.repository.UserRepository;
import com.rentcar.miniprojek.service.CustomerService;
import com.rentcar.miniprojek.service.CustomerValidationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@SpringBootTest
public class CustomerServiceTest {


    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CustomerValidationService customerValidationService;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private CustomerService customerService;
    @BeforeEach
    public void setup() {
        Mockito.reset(customerRepository);
    }

    @Test
    void doPositiveSaveCustomer() throws RegistrationException {
        CustomerRequest request = CustomerRequest.builder()
                .custName("hannis")
                .address("bekasi")
                .contactNo("0987")
                .drivingLicence("c")
                .username("hannis1")
                .password("1234")
                .build();

        customerValidationService.validateCustomer(request);

        Customer customer = new Customer();
        customer.setId(1);
        customer.setCustName(request.getCustName());
        customer.setAddress(request.getAddress());
        customer.setContactNo(request.getContactNo());
        customer.setDrivingLicence(request.getDrivingLicence());
        customer.setUsername(request.getUsername());
        customer.setPassword(request.getPassword());
        Mockito.lenient().when(customerRepository.save(customer)).thenReturn(customer);

        CustomerResponse response = customerService.saveCustomer(request);
        Assertions.assertEquals(response.getCustName(), request.getCustName());
    }

    @Test
    void doNegativeSaveCustomer() throws RegistrationException {
        CustomerRequest request = CustomerRequest.builder()
                .custName("hannis")
                .address("bekasi")
                .contactNo("0987")
                .drivingLicence("c")
                .username("hannis1")
                .password("1234")
                .build();

        customerService = Mockito.mock(CustomerService.class);
        doThrow(RegistrationException.class).when(customerService).saveCustomer(request);
        try {
            customerService.saveCustomer(request);
            Assertions.fail("Username already exists");
        }catch (RegistrationException e){
            Assertions.assertEquals("Username already exists",CustomerValidationService.USERNAME_ALREADY_EXISTS);
        }
    }

    @Test
    void doPositiveUpdateCustomer() {
        Integer id = 1;
        String validToken = "valid_token";

        CustomerRequest request = CustomerRequest.builder()
                .custName("hannis")
                .address("bekasi")
                .contactNo("0987")
                .drivingLicence("c")
                .username("hannis1")
                .password("1234")
                .build();

        Customer customer = new Customer();
        customer.setId(1);
        customer.setCustName(request.getCustName());
        customer.setAddress(request.getAddress());
        customer.setContactNo(request.getContactNo());
        customer.setDrivingLicence(request.getDrivingLicence());
        customer.setUsername(request.getUsername());
        customer.setPassword(request.getPassword());
        Mockito.lenient().when(customerRepository.save(customer)).thenReturn(customer);

        CustomerResponse response = customerService.saveCustomer(request);
        Assertions.assertEquals(response.getCustName(), request.getCustName());
    }

    @Test
    void doNegativeUpdateCustomer() {
        CustomerRequest request = CustomerRequest.builder()
                .custName("hannis")
                .address("bekasi")
                .contactNo("0987")
                .drivingLicence("c")
                .username("hannis1")
                .password("1234")
                .build();

        customerService = Mockito.mock(CustomerService.class);
        doThrow(RegistrationException.class).when(customerService).saveCustomer(request);
        try {
            customerService.saveCustomer(request);
            Assertions.fail("Username already exists");
        }catch (RegistrationException e){
            Assertions.assertEquals("Username already exists",CustomerValidationService.USERNAME_ALREADY_EXISTS);
        }
    }



}

