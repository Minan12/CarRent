package com.rentcar.miniprojek.service;

import com.rentcar.miniprojek.Exception.ResourceNotFoundException;
import com.rentcar.miniprojek.dto.order.OrderRequest;
import com.rentcar.miniprojek.dto.order.OrderResponse;
import com.rentcar.miniprojek.entity.Customer;
import com.rentcar.miniprojek.entity.Driver;
import com.rentcar.miniprojek.entity.Order;
import com.rentcar.miniprojek.entity.Vehicle;
import com.rentcar.miniprojek.repository.CustomerRepository;
import com.rentcar.miniprojek.repository.DriverRepository;
import com.rentcar.miniprojek.repository.OrderRepository;
import com.rentcar.miniprojek.repository.VehicleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
@SpringBootTest
public class OrderServiceTest {
    @Mock
    private TokenService tokenService;

    @Mock
    private OrderRepository orderRepository;
    @Mock
    private CustomerRepository customerRepository;
    @Mock
    private VehicleRepository vehicleRepository;
    @Mock
    private DriverRepository driverRepository;
    @InjectMocks
    private OrderService orderService;

    @Test
    public void testOrderExistValidation() {
        Order order = new Order();
        order.setId(1);

        when(orderRepository.findById(Mockito.anyInt())).thenReturn(Optional.of(order));

        assertEquals(order, orderService.orderExistValidation(1));
    }

    @Test
    public void testAddOrder() {
        Integer id = 1;
        String token = "1234";
        OrderRequest request = new OrderRequest(1,1,1,4);

        Customer customerMock = new Customer();
        customerMock.setId(request.getCustomerId());
        customerMock.setCustName("yudi");

        Vehicle vehicleMock = new Vehicle();
        vehicleMock.setId(request.getVehicleId());
        vehicleMock.setCarName("Alphard");
        vehicleMock.setPriceCar(BigDecimal.valueOf(250000));
        vehicleMock.setStatus("AVAILABLE");

        Driver driverMock = new Driver();
        driverMock.setId(request.getVehicleId());
        driverMock.setDriverName("Yanto");
        driverMock.setDriverPrice(BigDecimal.valueOf(30000));
        driverMock.setStatus("AVAILABLE");

        Order orderMock = new Order();
        orderMock.setId(id);
        orderMock.setHours(4);
        orderMock.setTotalPrice(vehicleMock.getPriceCar().multiply(BigDecimal.valueOf(request.getHours())).add(driverMock.getDriverPrice()));
        orderMock.setStatus("Inorder");
        orderMock.setCustomer(customerMock);
        orderMock.setVehicle(vehicleMock);
        orderMock.setDriver(driverMock);

        OrderResponse expectedData = OrderResponse.builder()
                .id(id)
                .custName(customerMock.getCustName())
                .driverName(driverMock.getDriverName())
                .carName(vehicleMock.getCarName())
                .priceCar(vehicleMock.getPriceCar())
                .hours(orderMock.getHours())
                .totalPrice(orderMock.getTotalPrice())
                .status(orderMock.getStatus())
                .build();

        when(customerRepository.findById(request.getCustomerId())).thenReturn(Optional.of(customerMock));
        when(driverRepository.findById(request.getDriverId())).thenReturn(Optional.of(driverMock));
        when(vehicleRepository.findById(request.getVehicleId())).thenReturn(Optional.of(vehicleMock));
        when(tokenService.getToken(token)).thenReturn(true);

        when(orderRepository.save(any())).thenReturn(orderMock);

        OrderResponse response = orderService.addOrder(token, request);

        Assertions.assertEquals(expectedData.getCustName(), response.getCustName());
        Assertions.assertEquals(expectedData.getDriverName(), response.getDriverName());
        Assertions.assertEquals(expectedData.getCarName(), response.getCarName());
        Assertions.assertEquals(expectedData.getPriceCar(), response.getPriceCar());
        Assertions.assertEquals(expectedData.getHours(), response.getHours());
        Assertions.assertEquals(expectedData.getTotalPrice(), response.getTotalPrice());
        Assertions.assertEquals(expectedData.getStatus(), response.getStatus());
    }
}
